<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> 
<!--<![endif]-->
	<head>
	
	<jsp:include page="include/meta.jsp"></jsp:include>
	<jsp:include page="include/header.jsp"></jsp:include>
	
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<% session.setAttribute("page", "searchbooking"); %>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">

			<jsp:include page="include/common_files/page_heading.jsp"></jsp:include>

			<!-- end:header-top -->

			<jsp:include page="include/common_files/booking_content.jsp"></jsp:include>
			

		<!-- Footer section -->
		<jsp:include page="include/common_files/page_footing.jsp"></jsp:include>
	

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->
<!-- modals -->
<%-- <jsp:include page="include/common_files/login-modale.jsp"></jsp:include> --%>
<%-- <jsp:include page="include/common_files/login-signup-modale.jsp"></jsp:include> --%>
<%-- <jsp:include page="include/common_files/signup-modale.jsp"></jsp:include> --%>

	<!-- jQuery -->
<jsp:include page="include/footer.jsp"></jsp:include>
<!-- jQuery -->
<script type="text/javascript">
function validateForm() {
	  var x = document.forms["reservationForm"]["userid"].value;
	  if ((x == "") || (x == null)) {
	    alert("S'inscrire ou connecter pour reserver le vol");
	    return false;
	  }
	}


</script>
<%-- <jsp:include page="include/common_files/flight-api-trakcer.jsp"></jsp:include> --%>
	</body>
</html>

