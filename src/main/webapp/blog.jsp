<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
			<jsp:include page="include/meta.jsp"></jsp:include>
		<jsp:include page="include/header.jsp"></jsp:include>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<% session.setAttribute("page", "blog"); %>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">

			<jsp:include page="include/common_files/page_heading.jsp"></jsp:include>

			<!-- end:header-top -->

			<jsp:include page="include/common_files/booking_content.jsp"></jsp:include>

		

		<div id="fh5co-blog-section" class="fh5co-section-gray">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
						<h3>Our Blog</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit est facilis maiores, perspiciatis accusamus asperiores sint consequuntur debitis.</p>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row row-bottom-padded-md">
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="fh5co-blog animate-box">
							<a href="#"><img class="img-responsive" src="images/place-1.jpg" alt=""></a>
							<div class="blog-text">
								<div class="prod-title">
									<h3><a href="#">Toutes les Destinations des Vols de la Mauritania Airlines</a></h3>
									<span class="posted_by">Sep. 15th</span>
									<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
									<p>Le r�seau national de la Mauritania Airlines s'�tend � travers les 3 principales villes de la Mauritanie. Depuis Nouakchott la capitale politique du pays, vous pourrez rejoindre Nouadhibou, une presqu'�le unique entre d�sert et mer ainsi que Zou�rate, ville du d�sert entour�e de vastes plateaux de minerais.</p>
									<p><a href="#">Voir Plus...</a></p>
								</div>
							</div> 
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="fh5co-blog animate-box">
							<a href="#"><img class="img-responsive" src="images/place-2.jpg" alt=""></a>
							<div class="blog-text">
								<div class="prod-title">
									<h3><a href="#">Planification des vacances</a></h3>
									<span class="posted_by">Sep. 15th</span>
									<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
									<p>Les vacances scolaires et horlog�res arrivent � grand pas ! Le boom de la parah�tellerie et un int�r�t croissant pour un tourisme plus local semblent se profiler. Pour en profiter �galement, il est n�cessaire de planifier dans un premier temps ses jours de cong� et ce, si possible, en harmonie avec les souhaits de son employeur et des autres collaborateurs.</p>
									<p><a href="#">Voir Plus...</a></p>
								</div>
							</div> 
						</div>
					</div>
					<div class="clearfix visible-sm-block"></div>
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="fh5co-blog animate-box">
							<a href="#"><img class="img-responsive" src="images/place-3.jpg" alt=""></a>
							<div class="blog-text">
								<div class="prod-title">
									<h3><a href="#">Visiter Dakar S�n�gal</a></h3>
									<span class="posted_by">Sep. 15th</span>
									<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
									<p>Vibrante, bruyante, tentaculaire, pollu�e ... les adjectifs ne manquent pas pour d�signer la capitale du S�n�gal. En 24 heures difficile de visiter Dakar dans les moindres recoins �videmment tant la ville est �tendue et le trafic souvent dense et laborieux. En r�alit�, il faudrait bien 24 heures pour simplement s'acclimater � la ville qui peut sembler "violente" surtout si comme nous, vous revenez du Sin� Saloum.</p>
									<p><a href="#">Voir Plus...</a></p>
								</div>
							</div> 
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="fh5co-blog animate-box">
							<a href="#"><img class="img-responsive" src="images/place-4.jpg" alt=""></a>
							<div class="blog-text">
								<div class="prod-title">
									<h3><a href="#">30% de r�duction pour voyager</a></h3>
									<span class="posted_by">Sep. 15th</span>
									<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
									<p>Le Black Friday est une tr�s bonne p�riode pour profiter des promotions sur les voyages et s'offrir des vacances � meilleur co�t. Cette ann�e, avec l'�pid�mie de coronavirus, l'heure n'est pas tr�s propice � partir en voyage. Par contre, c'est la bonne occasion pour s'�quiper pour de futurs voyages. Les promotions sur les mat�riels vont �tre nombreuses! Sinon vous pouvez miser sur l'am�lioration de la situation sanitaire l'ann�e prochaine et r�server en avance gr�ce � des promotions avantageuses. Par contre, v�rifiez bien les conditions d'annulation et de remboursement.

									</p>
									<p><a href="#">Voir Plus...</a></p>
								</div>
							</div> 
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="fh5co-blog animate-box">
							<a href="#"><img class="img-responsive" src="images/place-5.jpg" alt=""></a>
							<div class="blog-text">
								<div class="prod-title">
									<h3><a href="#">Planning for Vacation</a></h3>
									<span class="posted_by">Sep. 15th</span>
									<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
									<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
									<p><a href="#">Voir Plus...</a></p>
								</div>
							</div> 
						</div>
					</div>
					<div class="clearfix visible-sm-block"></div>
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="fh5co-blog animate-box">
							<a href="#"><img class="img-responsive" src="images/place-6.jpg" alt=""></a>
							<div class="blog-text">
								<div class="prod-title">
									<h3><a href="#">Toutes les Destinations des Vols de la Mauritania Airlines</a></h3>
									<span class="posted_by">Sep. 15th</span>
									<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
									<p>Au niveau National :  
Le r�seau national de la Mauritania Airlines s'�tend � travers les 3 principales villes de la Mauritanie. Depuis Nouakchott la capitale politique du pays, vous pourrez rejoindre Nouadhibou, une presqu'�le unique entre d�sert et mer ainsi que Zou�rate, ville du d�sert entour�e de vastes plateaux de minerais.
Au niveau International:
La Mauritania Airlines dessert pour vous plus de 10 grandes villes du continent Africain. Derri�re chaque voyage en Afrique, se cache tout un monde � d�couvrir. 
</p>
									<p><a href="#">Voir Plus...</a></p>
								</div>
							</div> 
						</div>
					</div>
					<div class="clearfix visible-md-block"></div>
				</div>

			</div>
		</div>
				<!-- fh5co-blog-section -->
		<jsp:include page="include/common_files/client_review.jsp"></jsp:include>

		<!-- Footer section -->
		<jsp:include page="include/common_files/page_footing.jsp"></jsp:include>	

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->

<jsp:include page="include/footer.jsp"></jsp:include>

	</body>
</html>

