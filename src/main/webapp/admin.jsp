<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> 
    <!--<![endif]-->
    <head>

        <jsp:include page="include/meta.jsp"></jsp:include>
        <jsp:include page="include/header.jsp"></jsp:include>

            <!-- FOR IE9 below -->
            <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
            <![endif]-->

        </head>
        <body>
        <% session.setAttribute("page", "admin");%>
        <div id="fh5co-wrapper">
            <div id="fh5co-page">

                <jsp:include page="include/common_files/admin_heading.jsp"></jsp:include>

                    <!-- end:header-top -->
                    <section class="content-header">
                        <h1>
                            Dashboard
                            <small>Panneau de control</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>150</h3>
                                    <p>Nouvelle r�servation</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="#" class="small-box-footer">Plus d'info<i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>53<sup style="font-size: 20px">%</sup></h3>
                                    <p>Taux de rebond</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="#" class="small-box-footer">Plus d'info<i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>44</h3>
                                    <p>Inscriptions</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="#" class="small-box-footer">Plus d'info<i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>65</h3>
                                    <p>visiteurs uniques</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="#" class="small-box-footer">Plus d'info<i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->
                    </div>
                    
                    <div class="col-md-6 col-sm-12 col-xs-12">                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Donut Chart Example
                        </div>
                        <div class="panel-body">
                            <div id="morris-donut-chart"><svg height="347" version="1.1" width="309" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Rapha�l 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="none" stroke="#0b62a4" d="M154.5,272.3333333333333A96.33333333333333,96.33333333333333,0,0,0,245.5416397003898,207.48858446763714" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#0b62a4" stroke="#ffffff" d="M154.5,275.3333333333333A99.33333333333333,99.33333333333333,0,0,0,248.37684647306628,208.46919782476078L286.33711492945724,221.59852110624962A139.5,139.5,0,0,1,154.5,315.5Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#3980b5" d="M245.5416397003898,207.48858446763714A96.33333333333333,96.33333333333333,0,0,0,68.09170999749297,133.41222558111596" stroke-width="2" opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#3980b5" stroke="#ffffff" d="M248.37684647306628,208.46919782476078A99.33333333333333,99.33333333333333,0,0,0,65.40079439187856,132.08596270993965L24.887564996239433,112.11833837167394A144.5,144.5,0,0,1,291.0624595505847,223.2328767014557Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#679dc6" d="M68.09170999749297,133.41222558111596A96.33333333333333,96.33333333333333,0,0,0,154.46973599126818,272.3333285794739" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#679dc6" stroke="#ffffff" d="M65.40079439187856,132.08596270993965A99.33333333333333,99.33333333333333,0,0,0,154.46879351348758,275.33332843142983L154.4561747832032,315.499993115951A139.5,139.5,0,0,1,29.372424338930117,114.32877649030114Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="154.5" y="166" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 800 15px Arial;" font-size="15px" font-weight="800" transform="matrix(1.5046,0,0,1.5046,-77.9538,-89.3063)" stroke-width="0.6646482122260668"><tspan dy="6" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">In-Store Sales</tspan></text><text x="154.5" y="186" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font: 14px Arial;" font-size="14px" transform="matrix(2.0069,0,0,2.0069,-155.6752,-179.2361)" stroke-width="0.4982698961937717"><tspan dy="5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">30</tspan></text></svg></div>
                        </div>
                    </div>            
                </div>

                    <div class="fh5co-hero">
                            <div class="desc">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="tabulation animate-box">

                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active">
                                                        <a href="#flights" aria-controls="flights" role="tab" data-toggle="tab">Vols</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#hotels" aria-controls="hotels" role="tab" data-toggle="tab">Hotels</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#Location" aria-controls="Location" role="tab" data-toggle="tab">Location</a>
                                                    </li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="flights">
                                                        <div class="row">
                                                            <div class="col-xxs-12 col-xs-12 mt" >
                                                                <label for="from">Numero de l'avion:</label>
                                                                <input type="text" class="form-control" id="from-place" placeholder="NJ-XXXX"/>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt">
                                                                <div class="input-field">
                                                                    <label for="from">Ville De Deppart:</label>
                                                                    <input type="text" class="form-control" id="from-place" placeholder="Nouakchott, MR"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt">
                                                                <div class="input-field">
                                                                    <label for="from">Ville d'arrive:</label>
                                                                    <input type="text" class="form-control" id="to-place" placeholder="Tokyo, Japan"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt alternate">
                                                                <div class="input-field">
                                                                    <label for="date-start">Date de depart:</label>
                                                                    <input type="text" class="form-control" id="date-start" placeholder="mm/dd/yyyy"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt alternate">
                                                                <div class="input-field">
                                                                    <label for="date-end">Date de l'arrive</label>
                                                                    <input type="text" class="form-control" id="date-end" placeholder="mm/dd/yyyy"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt alternate">
                                                                <div class="input-field">
                                                                    <label for="time-start">Heure de depart:</label>
                                                                    <input type="time" class="form-control" id="time-start" placeholder="XX:XX:XX"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt alternate">
                                                                <div class="input-field">
                                                                    <label for="time-end">Heure de l'arrive</label>
                                                                    <input type="time" class="form-control" id="time-end" placeholder="XX:XX:XX"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-12 mt" >
                                                                <label for="from">Capacite Total:</label>
                                                                <input type="text" class="col-xxs-12 col-xs-12 mt" id="from-place" placeholder="XXXXXXX"/>
                                                            </div>
                                                            <div >
                                                                <div class="col-xxs-12 col-xs-12 mt" >
                                                                    <div class="col-md-4"><label for="from">Classe Economique Qt:</label></div>
                                                                    <div class="col-xxs-12 col-xs-8 mt"><input type="text" class="form-control" id="from-place" placeholder="XXXX"/></div>
                                                                </div>
                                                                <div class="col-xxs-12 col-xs-10 mt" >
                                                                    <label for="from">Prix:</label>
                                                                    <input type="text" class="form-control" id="from-place" placeholder="1234567890"/>
                                                                </div>
                                                                <div class='col-xxs-12 col-xs-1 mt-1'>
                                                                    <label for="from">Devise</label>
                                                                    <select class="cs-select cs-skin-border">
                                                                        <option value="" disabled selected>MRU</option>
                                                                        <option value="1">MRU</option>
                                                                        <option value="2">USD</option>
                                                                        <option value="3">EUR</option>
                                                                    </select>
                                                                </div>

                                                            </div>
                                                            <div >
                                                                <div class="col-xxs-12 col-xs-12 mt" >
                                                                    <div class="col-md-4"><label for="from">Classe Business Qt:</label></div>
                                                                    <div class="col-xxs-12 col-xs-8 mt"><input type="text" class="form-control" id="from-place" placeholder="XXXX"/></div>
                                                                </div>
                                                                <div class="col-xxs-12 col-xs-10 mt" >
                                                                    <label for="from">Prix:</label>
                                                                    <input type="text" class="form-control" id="from-place" placeholder="1234567890"/>
                                                                </div>
                                                                <div class='col-xxs-12 col-xs-1 mt-1'>
                                                                    <label for="from">Devise</label>
                                                                    <select class="cs-select cs-skin-border">
                                                                        <option value="" disabled selected>MRU</option>
                                                                        <option value="1">MRU</option>
                                                                        <option value="2">USD</option>
                                                                        <option value="3">EUR</option>
                                                                    </select>
                                                                </div>

                                                            </div>
                                                            <div >
                                                                <div class="col-xxs-12 col-xs-12 mt" >
                                                                    <div class="col-md-4"><label for="from">Classe Premiere Classe Qt:</label></div>
                                                                    <div class="col-xxs-12 col-xs-8 mt"><input type="text" class="form-control" id="from-place" placeholder="XXXX"/></div>
                                                                </div>
                                                                <div class="col-xxs-12 col-xs-10 mt" >
                                                                    <label for="from">Prix:</label>
                                                                    <input type="text" class="form-control" id="from-place" placeholder="1234567890"/>
                                                                </div>
                                                                <div class='col-xxs-12 col-xs-1 mt-1'>
                                                                    <label for="from">Devise</label>
                                                                    <select class="cs-select cs-skin-border">
                                                                        <option value="" disabled selected>MRU</option>
                                                                        <option value="1">MRU</option>
                                                                        <option value="2">USD</option>
                                                                        <option value="3">EUR</option>
                                                                    </select>
                                                                </div>

                                                            </div>


                                                            <div class="col-xs-12">
                                                                <input type="submit" class="btn btn-primary btn-block" value="ajout de  vol">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="hotels">
                                                        <div class="row">
                                                            <div class="col-xxs-12 col-xs-12 mt">
                                                                <div class="input-field">
                                                                    <label for="from">Ville:</label>
                                                                    <input type="text" class="form-control" id="from-place" placeholder="Kiffa, MR"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt alternate">
                                                                <div class="input-field">
                                                                    <label for="date-start">date du debut</label>
                                                                    <input type="text" class="form-control" id="date-start" placeholder="mm/dd/yyyy"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt alternate">
                                                                <div class="input-field">
                                                                    <label for="date-end">date de fin:</label>
                                                                    <input type="text" class="form-control" id="date-end" placeholder="mm/dd/yyyy"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 mt">
                                                                <section>
                                                                    <label for="class">Chambre:</label>
                                                                    <select class="cs-select cs-skin-border">
                                                                        <option value="" disabled selected>1</option>
                                                                        <option value="economy">1</option>
                                                                        <option value="first">2</option>
                                                                        <option value="business">3</option>
                                                                    </select>
                                                                </section>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <input type="submit" class="btn btn-primary btn-block" value="ajout d'hotel">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="Location">
                                                        <div class="row">
                                                            <div class="col-xxs-12 col-xs-6 mt">
                                                                <div class="input-field">
                                                                    <label for="from">Ville:</label>
                                                                    <input type="text" class="form-control" id="from-place" placeholder="Nouadhibou, MR"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt">
                                                                <div class="input-field">
                                                                    <label for="from">Location:</label>
                                                                    <input type="text" class="form-control" id="to-place" placeholder="Bizert, Tunis"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt alternate">
                                                                <div class="input-field">
                                                                    <label for="date-start">date de debut:</label>
                                                                    <input type="text" class="form-control" id="date-start" placeholder="mm/dd/yyyy"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-6 mt alternate">
                                                                <div class="input-field">
                                                                    <label for="date-end">date de fin:</label>
                                                                    <input type="text" class="form-control" id="date-end" placeholder="mm/dd/yyyy"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 mt">
                                                                <section>
                                                                    <label for="class">categorie</label>
                                                                    <select class="cs-select cs-skin-border">
                                                                        <option value="" disabled selected>Econonomique</option>
                                                                        <option value="Econonomique">Econonomique</option>
                                                                        <option value="Compact">Compact</option>
                                                                        <option value="Intermediaire">Intermediaire</option>
                                                                        <option value="Intermediaire">Routiere</option>
                                                                        <option value="Intermediaire">LUXE</option>
                                                                        <option value="Intermediaire">Monospace</option>
                                                                        <option value="Intermediaire">Pick-up</option>
                                                                        <option value="Intermediaire">Sportive</option>
                                                                    </select>
                                                                </section>
                                                            </div>
                                                            <div class="col-xxs-12 col-xs-12 mt">
                                                                <div class="form-checkbox">
                                                                    <input type="checkbox" id="avancee2" name="checkavance2" onclick="document.getElementById('divavance2').style.display = (this.checked) ? 'block' : 'none';">
                                                                    <label for="avancee">Avance</label>
                                                                </div>
                                                            </div>
                                                            <div id="divavance2" style="display:none;">
                                                                <div class="col-xxs-12 col-xs-6 mt">
                                                                    <section>
                                                                        <label for="class">Type de Boite de Vitesse:</label>
                                                                        <select class="cs-select cs-skin-border">
                                                                            <option value="" disabled selected>Automatique</option>
                                                                            <option value="Automatique">Automatique</option>
                                                                            <option value="Mannuelle">Mannuelle</option>
                                                                        </select>
                                                                    </section>
                                                                </div>
                                                                <div class="col-xxs-12 col-xs-6 mt">
                                                                    <section>
                                                                        <label for="class">Type de carburant:</label>
                                                                        <select class="cs-select cs-skin-border">
                                                                            <option value="" disabled selected>ESSANCE</option>
                                                                            <option value="ESSANCE">ESSANCE</option>
                                                                            <option value="GAZOIL">GAZOIL</option>
                                                                            <option value="ELECTRIQUE">ELECTRIQUE</option>
                                                                            <option value="Hybride">Hybride</option>
                                                                        </select>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <input type="submit" class="btn btn-primary btn-block" value="Rechercher de paq">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div>
                       <table class="table" id="mytable">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">First</th>
                                    <th scope="col">Last</th>
                                    <th scope="col">Handle</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- Footer section -->
                <jsp:include page="include/common_files/admin_footing.jsp"></jsp:include>


                </div>
                <!-- END fh5co-page -->

            </div>
            <!-- END fh5co-wrapper -->
            <!-- modals -->
        <%-- <jsp:include page="include/common_files/login-modale.jsp"></jsp:include> --%>
        <%-- <jsp:include page="include/common_files/login-signup-modale.jsp"></jsp:include> --%>
        <%-- <jsp:include page="include/common_files/signup-modale.jsp"></jsp:include> --%>
        <!-- jQuery -->
        <jsp:include page="include/footer.jsp"></jsp:include>
            <!-- jQuery -->
        <%-- <jsp:include page="include/common_files/flight-api-trakcer.jsp"></jsp:include> --%>
    </body>
</html>