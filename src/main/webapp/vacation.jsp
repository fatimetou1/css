<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<jsp:include page="include/meta.jsp"></jsp:include>
<jsp:include page="include/header.jsp"></jsp:include>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>
<% session.setAttribute("page", "vacation"); %>
	<div id="fh5co-wrapper">
		<div id="fh5co-page">

			<jsp:include page="include/common_files/page_heading.jsp"></jsp:include>

			<!-- end:header-top -->

			<jsp:include page="include/common_files/booking_content.jsp"></jsp:include>


			<div id="fh5co-tours" class="fh5co-section-gray">
				<div class="container">
					<div class="row">
						<div
							class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
							<h3>Sites touristiques</h3>
							<p>Far far away, behind the word mountains, far from the
								countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-1.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>New York</h3>
									<span>3 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-2.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Philippines</h3>
									<span>4 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-3.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Hongkong</h3>
									<span>2 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant <i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-4.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>New York</h3>
									<span>3 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-5.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Philippines</h3>
									<span>4 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-6.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Hongkong</h3>
									<span>2 nuits + Vols 4*Hotel</span> <span class="price">$1,000</span>
									<a class="btn btn-primary btn-outline" href="#">Book Now <i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
					<!-- fh5co-blog-section -->
		<jsp:include page="include/common_files/client_review.jsp"></jsp:include>

		<!-- Footer section -->
		<jsp:include page="include/common_files/page_footing.jsp"></jsp:include>


		</div>
		<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->

	<jsp:include page="include/footer.jsp"></jsp:include>

</body>
</html>

