<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<jsp:include page="include/meta.jsp"></jsp:include>
<jsp:include page="include/header.jsp"></jsp:include>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>
<% session.setAttribute("page", "hotel"); %>
	<div id="fh5co-wrapper">
		<div id="fh5co-page">

			<jsp:include page="include/common_files/page_heading.jsp"></jsp:include>

			<!-- end:header-top -->

			<jsp:include page="include/common_files/booking_content.jsp"></jsp:include>


			<div id="fh5co-tours" class="fh5co-section-gray">
				<div class="container">
					<div class="row">
						<div
							class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
							<h3>H�tel populaire &amp; destinations auberge</h3>
							<p>vous pouvez b�n�ficier des meilleures offres d'h�tels du monde entier en un seul clic</p>
						</div>
					</div>
					<div class="row row-bottom-padded-md">
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-1.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>New York</h3>
									<span>3 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant <i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-2.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Philippines</h3>
									<span>4 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant <i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-3.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Hongkong</h3>
									<span>2 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-1.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>New York</h3>
									<span>3 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-2.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Philippines</h3>
									<span>4 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant <i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-3.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Hongkong</h3>
									<span>2 nights</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 animate-box">
							<h2 class="heading-title">Guide de r�servation de voyage pour r�server un h�tel parfait</h2>
						</div>
						<div class="col-md-6 animate-box">
							<p>Vous avez pr�vu de partir en voyage et vous avez besoin d un coup de main pour tout planifier?

Et oui, organiser un voyage n'est pas toujours facile, surtout si vous n'avez pas l'habitude. Heureusement, l'organisation de voyage, c'est ma sp�cialit� et je suis l� pour vous aider!

Dans ce guide, je vais vous donner tous les conseils et astuces n�cessaires � la pr�paration de votre s�jour: comment cr�er votre itin�raire d�taill�, r�server votre vol, votre h�bergement, louer votre voiture et bien plus!

Et ce n'est pas tout, sur Voyage Tips, je vous propose �galement des guides complets et gratuits sur des dizaines de destinations. Vous y trouverez toutes les infos utiles pour planifier votre voyage de r�ve facilement et rapidement.

Alors, pr�ts � organiser votre voyage de A � Z?</p>
							<p>Premi�re �tape dans l'organisation d'un voyage: choisir sa destination.

Et figurez-vous que ce n'est pas forc�ment si simple que �a!

D'abord parce que le monde est vaste, ensuite parce que vous n'aurez peut-�tre pas forc�ment les m�mes attentes que la ou les personnes avec qui vous allez partir, et surtout parce qu'il n'est pas possible de voyager partout � n'importe quelle p�riode de l'ann�e.

Pour pr�parer votre voyage et choisir le pays o� vous voulez aller, il faut donc d'abord se poser quelques questions:

Vous voulez partir en vacances au soleil, profiter des plages?
Vous cherchez plus une destination culturelle avec des visites de mus�es, de lieux historiques?
Vous pr�f�rez rester en Europe ou explorer des destinations plus lointaines?
Vous cherchez des endroits touristiques ou plus hors des sentiers battus?
Combien d'heures d'avion �tes-vous pr�ts � faire?</p>
							<a href="#">Read More <i class="icon-arrow-right22"></i></a>
						</div>
						<div class="col-md-6 animate-box">
							<img class="img-responsive" src="images/cover_bg_2.jpg"
								alt="travel">
						</div>
					</div>
				</div>
			</div>
		<!-- fh5co-blog-section -->
		<jsp:include page="include/common_files/client_review.jsp"></jsp:include>

		<!-- Footer section -->
		<jsp:include page="include/common_files/page_footing.jsp"></jsp:include>


		</div>
		<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->


	<jsp:include page="include/footer.jsp"></jsp:include>

</body>
</html>

