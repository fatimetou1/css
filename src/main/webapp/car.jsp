<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<jsp:include page="include/meta.jsp"></jsp:include>
		<jsp:include page="include/header.jsp"></jsp:include>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<% session.setAttribute("page", "car"); %>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">

			<jsp:include page="include/common_files/page_heading.jsp"></jsp:include>

			<!-- end:header-top -->

			<jsp:include page="include/common_files/booking_content.jsp"></jsp:include>
		

		<div id="fh5co-car" class="fh5co-section-gray">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
						<h3>Location de voitures en Mauritanie</h3>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
					</div>
				</div>
				<div class="row row-bottom-padded-md">
					<div class="col-md-6 animate-box">
						<div class="car">
							<div class="one-4">
								<h3>Economie</h3>
								<span class="price">$100<small> / jour</small></span>
								<span class="price">$200<small> / semaine</small></span>
								<span class="price">$250<small> / mois.</small></span>
								<span class="price">$350<small> Total (TTC)</small></span>
							</div>
							<div class="one-1" style="background-image: url(images/car-1.jpg);">
							</div>
						</div>
					</div>
					<div class="col-md-6 animate-box">
						<div class="car">
							<div class="one-4">
								<h3>Economie</h3>
								<span class="price">$100<small> / jour</small></span>
								<span class="price">$200<small> / semaine</small></span>
								<span class="price">$250<small> / mois.</small></span>
								<span class="price">$350<small> Total (TTC)</small></span>
							</div>
							<div class="one-1" style="background-image: url(images/car-2.jpg);">
							</div>
						</div>
					</div>
					<div class="col-md-6 animate-box">
						<div class="car">
							<div class="one-4">
								<h3>Luxury</h3>
								<span class="price">$100<small> / jour</small></span>
								<span class="price">$200<small> / semaine</small></span>
								<span class="price">$250<small> / mois.</small></span>
								<span class="price">$350<small> Total (TTC)</small></span>
							</div>
							<div class="one-1" style="background-image: url(images/car-3.jpg);">
							</div>
						</div>
					</div>
					<div class="col-md-6 animate-box">
						<div class="car">
							<div class="one-4">
								<h3>Economie</h3>
								<span class="price">$100<small> / jour</small></span>
								<span class="price">$200<small> / semaine</small></span>
								<span class="price">$250<small> / mois.</small></span>
								<span class="price">$350<small> Total (TTC)</small></span>
							</div>
							<div class="one-1" style="background-image: url(images/car-4.jpg);">
							</div>
						</div>
					</div>
					<div class="col-md-6 animate-box">
						<div class="car">
							<div class="one-4">
								<h3>Economy</h3>
								<span class="price">$100<small> / day</small></span>
								<span class="price">$200<small> / week</small></span>
								<span class="price">$250<small> / mos.</small></span>
								<span class="price">$350<small> Total (Tax incl.)</small></span>
							</div>
							<div class="one-1" style="background-image: url(images/car-5.jpg);">
							</div>
						</div>
					</div>
					<div class="col-md-6 animate-box">
						<div class="car">
							<div class="one-4">
								<h3>Economy</h3>
								<span class="price">$100<small> / jour</small></span>
								<span class="price">$200<small> / semaine</small></span>
								<span class="price">$250<small> / mois.</small></span>
								<span class="price">$350<small> Total (TTC)</small></span>
							</div>
							<div class="one-1" style="background-image: url(images/car-6.jpg);">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
				<!-- fh5co-blog-section -->
		<jsp:include page="include/common_files/client_review.jsp"></jsp:include>

		<!-- Footer section -->
		<jsp:include page="include/common_files/page_footing.jsp"></jsp:include>
	

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->


<jsp:include page="include/footer.jsp"></jsp:include>

	</body>
</html>

