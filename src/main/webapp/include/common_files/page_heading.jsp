<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<header id="fh5co-header-section" class="sticky-banner">
			<div class="container">
				<div class="nav-header">
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
					<!--  <div class="site-logo col-6">
                <span class="navbar-logo">
                    <a href="https://mobiri.se">
                        <img src="images/icon.png" alt="RIM-Aire" style="height: 3.8rem;">
                    </a>
                </span>
                <span class="navbar-caption-wrap"><a class="navbar-caption text-black display-7" href="">RIM-Aire</a></span>
            </div> -->
					<h1 id="fh5co-logo"><a href="<%=request.getContextPath()%>/index"><img src="images/icon.png" alt="RIM-Aire" style="height: 3.8rem;"> RIM Aire</a></h1>
					<!-- START #fh5co-menu-wrap <i class="icon-airplane">-->
					<nav id="fh5co-menu-wrap" role="navigation">
						<ul class="sf-menu" id="fh5co-primary-menu">
							<c:choose>
							<c:when test="${sessionScope.page=='index' }"><li class="active"> <a href="<%=request.getContextPath()%>/index">Accueil</a></li></c:when>
							<c:otherwise><li > <a href="<%=request.getContextPath()%>/index">Accueil</a></li></c:otherwise>
							</c:choose>
							<c:choose>
							<c:when test="${sessionScope.page=='vacation' }">
							<li class="active">
								<a href="<%=request.getContextPath()%>/vacation" class="fh5co-sub-ddown">Services</a>
								<ul class="fh5co-sub-menu">
									<li class="active"><a href="<%=request.getContextPath()%>/vacation">Vacation</a></li>
									<li ><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
									<li ><a href="<%=request.getContextPath()%>/car">Voiture</a></li>
									<li ><a href="<%=request.getContextPath()%>/blog">Blog</a></li>
									<li ><a href="<%=request.getContextPath()%>/contact">Contact &amp; Qui est nous</a></li>
								</ul>
							</li>
							</c:when>
							<c:when test="${sessionScope.page=='hotel' }">
							<li class="active">
								<a href="<%=request.getContextPath()%>/vacation" class="fh5co-sub-ddown">Services</a>
								<ul class="fh5co-sub-menu">
									<li ><a href="<%=request.getContextPath()%>/vacation">Vacation</a></li>
									<li class="active"><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
									<li ><a href="<%=request.getContextPath()%>/car">Voiture</a></li>
									<li ><a href="<%=request.getContextPath()%>/blog">Blog</a></li>
									<li ><a href="<%=request.getContextPath()%>/contact">Contact &amp; Qui est nous</a></li>
								</ul>
							</li>
							</c:when>
							<c:when test="${sessionScope.page=='car' }">
							<li class="active">
								<a href="<%=request.getContextPath()%>/vacation" class="fh5co-sub-ddown">Services</a>
								<ul class="fh5co-sub-menu">
									<li ><a href="<%=request.getContextPath()%>/vacation">Vacation</a></li>
									<li ><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
									<li class="active"><a href="<%=request.getContextPath()%>/car">Voiture</a></li>
									<li ><a href="<%=request.getContextPath()%>/blog">Blog</a></li>
									<li ><a href="<%=request.getContextPath()%>/contact">Contact &amp; Qui est nous</a></li>
								</ul>
							</li>
							</c:when>
							<c:when test="${sessionScope.page=='blog' }">
							<li class="active">
								<a href="<%=request.getContextPath()%>/vacation" class="fh5co-sub-ddown">Services</a>
								<ul class="fh5co-sub-menu">
									<li ><a href="<%=request.getContextPath()%>/vacation">Vacation</a></li>
									<li ><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
									<li ><a href="<%=request.getContextPath()%>/car">Voiture</a></li>
									<li class="active"><a href="<%=request.getContextPath()%>/blog">Blog</a></li>
									<li ><a href="<%=request.getContextPath()%>/contact">Contact &amp; Qui est nous</a></li>
								</ul>
							</li>
							</c:when>
							<c:when test="${sessionScope.page=='contact' }">
							<li class="active">
								<a href="<%=request.getContextPath()%>/vacation" class="fh5co-sub-ddown">Services</a>
								<ul class="fh5co-sub-menu">
									<li ><a href="<%=request.getContextPath()%>/vacation">Vacation</a></li>
									<li ><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
									<li ><a href="<%=request.getContextPath()%>/car">Voiture</a></li>
									<li ><a href="<%=request.getContextPath()%>/blog">Blog</a></li>
									<li class="active"><a href="<%=request.getContextPath()%>/contact">Contact &amp; Qui est nous</a></li>
								</ul>
							</li>
							</c:when>
							<c:otherwise>
							<li >
								<a href="<%=request.getContextPath()%>/vacation" class="fh5co-sub-ddown">Services</a>
								<ul class="fh5co-sub-menu">
									<li ><a href="<%=request.getContextPath()%>/vacation">Vacation</a></li>
									<li ><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
									<li ><a href="<%=request.getContextPath()%>/car">Voiture</a></li>
									<li ><a href="<%=request.getContextPath()%>/blog">Blog</a></li>
									<li ><a href="<%=request.getContextPath()%>/contact">Contact &amp; Qui est nous</a></li>
								</ul>
							</li>
							</c:otherwise>
							</c:choose>
							
							<c:choose>
							<c:when test="${sessionScope.page=='flight' }"><li class="active"><a href="<%=request.getContextPath()%>/flight">Vols</a></li></c:when>
							<c:otherwise><li ><a href="<%=request.getContextPath()%>/flight">Vols</a></li></c:otherwise>
							</c:choose>
							
							 <li><a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Connecter</a></li>
							 <li> <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalRegisterForm">Enregistrer</a></li>
							 <!-- <li><a href="" class="btn btn-default btn-rounded my-3" data-toggle="modal" data-target="#modalLRForm">Connecter &amp; Enregistrer</a></li> -->
						</ul>
					</nav>
				</div>
			</div>
		</header>
<%-- 		class='<% session.getAttribute("page").toString().equals("index")? "active" : ""; %>' --%>