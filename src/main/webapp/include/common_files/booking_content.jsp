<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

		<div class="fh5co-hero">
			<div class="fh5co-overlay"></div>
			
			<div class="fh5co-cover" data-stellar-background-ratio="1.5" style="background-image: url(images/cover_bg_1.jpg);">
				<%-- <c:if test="${bookingsearchvide != null}">
									<div class="alert alert-warning"><c:out value="${bookingsearchvide}" /></div>
								</c:if> --%>
				<div class="desc">
				
					<div class="container">
					
						<div class="row">
							<div class="col-sm-5 col-md-5">
							<form method="post" action="<%=request.getContextPath()%>/recherche_des_vols">
								<div class="tabulation animate-box">

								  <!-- Nav tabs -->
								   <ul class="nav nav-tabs" role="tablist">
								      <li role="presentation" class="active">
								      	<a href="#flights" aria-controls="flights" role="tab" data-toggle="tab">Vols</a>
								      </li>
								     <!--  <li role="presentation">
								    	   <a href="#hotels" aria-controls="hotels" role="tab" data-toggle="tab">Hotels</a>
								      </li> -->
								      <!-- <li role="presentation">
								    	   <a href="#packages" aria-controls="packages" role="tab" data-toggle="tab">Paquets</a>
								      </li> -->
								   </ul>

								   <!-- Tab panes -->
								   
									<div class="tab-content">
									<!-- <form action="cherchevole" method="post" > -->
									 <div role="tabpanel" class="tab-pane active" id="flights"><!-- first form -->
										<div class="row">
											<div class="col-xxs-12 col-xs-12 mt">
												<div class="form-checkbox">
													<label for="roundtrip">
														<input type="radio" id="roundtrip" onclick="document.getElementById('rdate').style.display = (this.checked) ? 'block' : 'none';" name="flight-type">
														<span></span>aller-retour
													</label>
													<label for="one-way">
														<input type="radio" id="one-way" onclick="document.getElementById('rdate').style.display = (this.checked) ? 'none' : 'block';" name="flight-type">
														<span></span>sens unique 
													</label>
												</div>
											</div>
											<!-- first form -->
											<div class="col-xxs-12 col-xs-6 mt">
												<div class="input-field">
													<label for="from">De:</label>
													<select class="form-control" id="from-place" name="departcity" required>
													<option></option>
													 
													</select>
													<!-- <input type="text" class="form-control" id="from-place" name="departcity" placeholder="Nouakchott, MR"/> -->
												</div>
											</div>
											<div class="col-xxs-12 col-xs-6 mt">
												<div class="input-field">
													<label for="from">A:</label>
													<select class="form-control" id="from-place" name="arvcity" required>
													<option></option>
													 
													</select>
													<!-- <input type="text" class="form-control" id="to-place" name="arvcity" placeholder="Tokyo, Japan"/> -->
												</div>
											</div>
											<div class="col-xxs-12 col-xs-6 mt alternate form_datetime">
												<div class="input-field">
													<label for="date-start">Date de depart:</label>
													<input type="text" class="form-control" name="departtime" id="date-start" placeholder="mm/dd/yyyy"/ required>
												</div>
											</div>
											<div id="rdate" class="col-xxs-12 col-xs-6 mt alternate" style="display:none;">
												<div class="input-field">
													<label for="date-end">Date de Retour:</label>
													<input type="text" class="form-control" id="date-end" name="arvtime" placeholder="mm/dd/yyyy"/>
												</div>
											</div>
											<div class="col-sm-12 mt">
												<section>
													<div class="input-field">
													<label for="fltcls">Categorie:</label>
													<select class="cs-select cs-skin-border" name="fltclass" id="fltcls" required>
														<option></option>
														
														<!-- <option value="economy">Economie</option>selected
														<option value="business">Classe Affaire</option>
														<option value="first">Premiere classe</option> -->
														
													</select>
													</div>
												</section>
											</div>
											<div class="col-xxs-12 col-xs-12 mt">
												<div class="form-checkbox">
														<input type="checkbox" id="avancee" name="checkavance" onclick="document.getElementById('divavance').style.display = (this.checked) ? 'block' : 'none';">
  														<label for="avancee">Avance</label>
												</div>
											</div>
											<div id="divavance" style="display:none;">
											<div class="col-xxs-12 col-xs-6 mt">
												<section>
													<label for="class">Adulte:</label>
													<select class="cs-select cs-skin-border">
														<option value="" disabled selected>1</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
													</select>
												</section>
											</div>
											<div class="col-xxs-12 col-xs-6 mt">
												<section>
													<label for="class">Enfant:</label>
													<select class="cs-select cs-skin-border">
														<option value="" disabled selected>1</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
													</select>
												</section>
											</div>
											</div>
											<div class="col-xs-12">
												<input type="submit" class="btn btn-primary btn-block" name="actiontosearch" value="Recherche de vol">Recherche de vol</input>
											</div>
											<!-- end first form -->
										</div>
									 </div><!-- end first form -->
									 <!-- </form>
									 <form method="post" action="cherchehotel"> -->
									 <!-- 
									 <div role="tabpanel" class="tab-pane" id="hotels">
									 	<div class="row">
											<div class="col-xxs-12 col-xs-12 mt">
												<div class="input-field">
													<label for="from">Ville:</label>
													<input type="text" class="form-control" id="from-place" placeholder="Kiffa, MR"/>
												</div>
											</div>
											<div class="col-xxs-12 col-xs-6 mt alternate">
												<div class="input-field">
													<label for="date-start">date du depart:</label>
													<input type="text" class="form-control" id="date-start" placeholder="mm/dd/yyyy"/>
												</div>
											</div>
											<div class="col-xxs-12 col-xs-6 mt alternate">
												<div class="input-field">
													<label for="date-end">date de Retour:</label>
													<input type="text" class="form-control" id="date-end" placeholder="mm/dd/yyyy"/>
												</div>
											</div>
											<div class="col-sm-12 mt">
												<section>
													<label for="class">Chambre:</label>
													<select class="cs-select cs-skin-border">
														<option value="" disabled selected>1</option>
														<option value="economy">1</option>
														<option value="first">2</option>
														<option value="business">3</option>
													</select>
												</section>
											</div>
											<div class="col-xxs-12 col-xs-12 mt">
												<div class="form-checkbox">
														<input type="checkbox" id="avancee1" name="checkavance1" onclick="document.getElementById('divavance1').style.display = (this.checked) ? 'block' : 'none';">
  														<label for="avancee">Avance</label>
												</div>
											</div>
											<div id="divavance1" style="display:none;">
											<div class="col-xxs-12 col-xs-6 mt">
												<section>
													<label for="class">Adulte:</label>
													<select class="cs-select cs-skin-border">
														<option value="" disabled selected>1</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
													</select>
												</section>
											</div>
											<div class="col-xxs-12 col-xs-6 mt">
												<section>
													<label for="class">Enfants:</label>
													<select class="cs-select cs-skin-border">
														<option value="" disabled selected>1</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
													</select>
												</section>
											</div>
											</div>
											<div class="col-xs-12">
												<input type="submit" class="btn btn-primary btn-block" name="actiontosearch" value="Recherche d'hotel">Recherche d'hotel</input>
											</div>
										</div>
									 </div> --><!-- end second form -->
									 <!-- </form> -->
<!-- 									 <div role="tabpanel" class="tab-pane" id="packages">
									 	<div class="row">
											<div class="col-xxs-12 col-xs-6 mt">
												<div class="input-field">
													<label for="from">Ville:</label>
													<input type="text" class="form-control" id="from-place" placeholder="Nouadhibou, MR"/>
												</div>
											</div>
											<div class="col-xxs-12 col-xs-6 mt">
												<div class="input-field">
													<label for="from">Destination:</label>
													<input type="text" class="form-control" id="to-place" placeholder="Bizert, Tunis"/>
												</div>
											</div>
											<div class="col-xxs-12 col-xs-6 mt alternate">
												<div class="input-field">
													<label for="date-start">Partir:</label>
													<input type="text" class="form-control" id="date-start" placeholder="mm/dd/yyyy"/>
												</div>
											</div>
											<div class="col-xxs-12 col-xs-6 mt alternate">
												<div class="input-field">
													<label for="date-end">Revenir:</label>
													<input type="text" class="form-control" id="date-end" placeholder="mm/dd/yyyy"/>
												</div>
											</div>
											<div class="col-sm-12 mt">
												<section>
													<label for="class">Chambre:</label>
													<select class="cs-select cs-skin-border">
														<option value="" disabled selected>1</option>
														<option value="economy">1</option>
														<option value="first">2</option>
														<option value="business">3</option>
													</select>
												</section>
											</div>
											<div class="col-xxs-12 col-xs-12 mt">
												<div class="form-checkbox">
														<input type="checkbox" id="avancee2" name="checkavance2" onclick="document.getElementById('divavance2').style.display = (this.checked) ? 'block' : 'none';">
  														<label for="avancee">Avance</label>
												</div>
											</div>
											<div id="divavance2" style="display:none;">
											<div class="col-xxs-12 col-xs-6 mt">
												<section>
													<label for="class">Adulte:</label>
													<select class="cs-select cs-skin-border">
														<option value="" disabled selected>1</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
													</select>
												</section>
											</div>
											<div class="col-xxs-12 col-xs-6 mt">
												<section>
													<label for="class">Enfants:</label>
													<select class="cs-select cs-skin-border">
														<option value="" disabled selected>1</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
													</select>
												</section>
											</div>
											</div>
											<div class="col-xs-12">
												<input type="submit" class="btn btn-primary btn-block" value="Rechercher de paq">
											</div>
										</div>
									 </div> --><!-- end third form -->
									</div>

								</div>
								</form>
								
							</div>
							<div class="animate-box">
								<c:choose>
								<c:when >
									<div class="col-sm-7 col-sm-push-1 col-md-7 col-md-push-1">
										<p>Fabrique pour <a href="#" target="_blank" class="fh5co-site-name">RIM Aire</a></p>
										<h3>logiciel pour les r�servations de billets d'avion pour la compagnie a�rienne RIM-AIR</h3>
										<h3>Voyagez en toute securite et a des tarifs raisonnables</h3>
										<span class="price">$150</span>
									</div>
								</c:when>
								<c:when >
									<div class="alert alert-warning col-sm-6 col-sm-push-1 col-md-6 col-md-push-1"><c:out  /></div>
								</c:when>
								<c:when >
									<div class="col-sm-7 col-sm-push-1 col-md-7 col-md-push-1">
										<%-- <c:set var="nrow" value="0"></c:set> desc2--%>
							 		 	<table class="table table-light table-borderless text-center" id="listresto" width="100%" >
							 		 		<tr>
							 		 		
							 		 		<%// List<flight> rechbooking = request.getAttribute("bookingsearch").; %>
											
												<% //int rechbooking = request.getAttribute("bookingsearch").; %>	
												<% // int idfly = ardao.selectAirline(0).getName_airline(); style="background-color:rgba(0, 0, 0, 0.06);" %> 
												<td>
												<form method="post"  name="reservationForm" onsubmit="return validateForm()">
												<div  class="card shadow shadow-sm">
												 <!-- bg-transparent -->
												  <div class="card-header border-success" style="background-color: rgba(109, 159, 96, 1);"><c:out  /></div>
												  <div class="card-body text-success">
												    <h4 class="card-title"> N�: <c:out  /></h4>
												    
												    <hr/>
												    <!-- <p class="card-text"></p> -->
												    <div class="row ">
													    <div class="col-sm-12 "><span class="float-left">Departure:</span></div>
													    <div class="col-sm-12">
													    	<span class="float-left"> <c:out /></span>
													    </div>
													    <div class="col-sm-12">
															<span class="float-left"> <c:out  /> </span>
														</div>
												    </div>
												    <!-- <p class="card-text"></p> --><hr/>
												    <div class="row">
												    	<div class="col-sm-12 " ><span class="float-left">Arrive:</span></div>
												    	<div class="col-sm-12">
														 	<span class="float-left"> <c:out  /> </span>
														</div>
														<div class="col-sm-12">
															<span class="float-left"> <c:out  /> </span>
														</div>
												    </div>
												   <%--  <div class="col-sm-6"><span class="float-left">Departure:</span></div>
												    <div class="col-sm-6 " ><span class="float-left">Arrive:</span></div>
												 	<div class="col-sm-6"><span > <c:out value="${fly.depCity}" /></span> 
													 </div>
													 
													 <div class="col-sm-6">
													 <span > <c:out value="${fly.arvCity}" /> </span>
													 </div>
													 <div class="col-sm-6">
													<span> <c:out value="${fly.depDate}" /> </span>
													</div>
													<div class="col-sm-6">
													<span > <c:out value="${fly.arvDate}" /> </span>
													</div> --%>
													 <!-- <p class="card-text"></p> -->
													 <hr/>
													<div class="col-sm-12 ">
													<span class="bg-success float-right">prix: </span><span class="float-left bg-primary"><c:out  /> MRU </span>
													</div>
													 <hr/>
													<div class="col-sm-6">
													<span>Repas gratuits:  </span>
													</div>
													<div class="col-sm-6">
													<span>Remboursable:  <c:out  /> </span>
													</div>
												  </div>
												<%-- <c:choose>
													<c:when test="${sessionScope.userlogin != null }"> --%>
														<input type="hidden" name="userid" ">
													  	<div class="card-footer border-success">
													  		<input type="submit" name="btnreservation" value="Reserver" class="btn btn-primary" />
													  	</div>
													<%-- </c:when> --%>
													
												<%-- </c:choose> --%>
												</div>
												</form>
												</td>
												
											
										</table>
									</div>
								</c:when>
								</c:choose>
								
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>