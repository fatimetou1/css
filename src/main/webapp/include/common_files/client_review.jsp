		<div id="fh5co-testimonial" style="background-image:url(images/img_bg_1.jpg);">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Clients satisfaits</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="box-testimony animate-box">
						<blockquote>
							<span class="quote"><span><i class="icon-quotes-right"></i></span></span>
							<p>&ldquo;Les services disponibles sont simples et vari�s
Lors de mon dernier voyage d'affaires, j'ai pu r�server un vol, obtenir une chambre dans un h�tel donnant sur la rue principale et louer une voiture et autres services en un clic sans se deplacer.&rdquo;</p>
						</blockquote>
						<p class="author">Med Horma, CEO <a href="" target="_blank">MedHormaCompanyLtd</a> <span class="subtext">Directeur de cr�ation</span></p>
					</div>
					
				</div>
				<div class="col-md-4">
					<div class="box-testimony animate-box">
						<blockquote>
							<span class="quote"><span><i class="icon-quotes-right"></i></span></span>
							<p>&ldquo;J'ai pris l'avion avec vos compagnies a�riennes l'�t� dernier et l'�quipage �tait tr�s courtois et professionnel et quand je suis arriv�, j'ai �t� re�u et j'ai fourni une voiture pour le transport jusqu'� l'h�tel qui �tait pr�-r�serv�..&rdquo;</p>
						</blockquote>
						<p class="author">Abderrahmane Yeslim, CEO <a href="" target="_blank">YaSoft Ltd</a> <span class="subtext">fondateur et directeur</span></p>
					</div>
					
					
				</div>
				<div class="col-md-4">
					<div class="box-testimony animate-box">
						<blockquote>
							<span class="quote"><span><i class="icon-quotes-right"></i></span></span>
							<p>&ldquo;C'est une belle application et dispose de nombreux services
Ce groupe m�rite une bonne note et beaucoup d'appr�ciation
Veuillez accepter de travailler avec nous apr�s avoir perfectionn� leurs talents. Je crois qu'ils auront un avenir prometteur.&rdquo;</p>
						</blockquote>
						<p class="author">Rod Johnson, Founder <a href="#">Spring</a> <span class="subtext">Directeur de cr�ation</span></p>
					</div>
					
				</div>
			</div>
		</div>
	</div>