<footer>
			<div id="footer">
				<div class="container">
					<div class="row row-bottom-padded-md">
						<div class="col-md-2 col-sm-2 col-xs-12 fh5co-footer-link">
							<h3>� propos de RIM AIRELINES</h3>
							<p>Mauritania Airlines Booking vous offre la possibilit� de concevoir votre itin�raire � l'int�rieur et � l'ext�rieur du pays. Sur cette m�me plateforme, vous pouvez r�server et g�rer tous les services dont vous avez besoin lors de votre voyage: Vol, H�tel, Voiture de Location. Le tout, sur une seule plateforme !</p>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 fh5co-footer-link">
							<h3>Meilleurs itin�raires</h3>
							<ul>
								<li><a href="#">Vols vers Senegal</a></li>
								<li><a href="#">Vols vers Duba�</a></li>
								<li><a href="#">Vols vers Bangkok</a></li>
								<li><a href="#">Vols vers Tokyo</a></li>
								<li><a href="#">Vols vers New York</a></li>
							</ul>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 fh5co-footer-link">
							<h3>Top H�tels</h3>
							<ul>
								<li><a href="#">H�tel Boracay</a></li>
								<li><a href="#">H�tel Duba�</a></li>
								<li><a href="#">H�tel Singapour</a></li>
								<li><a href="#">H�tel S�n�gal</a></li>
							</ul>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 fh5co-footer-link">
							<h3>Interest</h3>
							<ul>
								<li><a href="#">plages</a></li>
								<li><a href="#">Voyage en famille</a></li>
								<li><a href="#">Voyage � petit budget</a></li>
								<li><a href="#">Nourriture &amp; boisson</a></li>
								<li><a href="#">Lune de miel et romance</a></li>
							</ul>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 fh5co-footer-link">
							<h3>Meilleurs endroits</h3>
							<ul>
								<li><a href="#">Plage de Boracay</a></li>
								<li><a href="#">Dubai</a></li>
								<li><a href="#">Singapour</a></li>
								<li><a href="#">Hong Kong</a></li>
							</ul>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12 fh5co-footer-link">
							<h3>Abordable</h3>
							<ul>
								<li><a href="#">Nourriture &amp; boisson</a></li>
								<li><a href="#">Vols tarifaires</a></li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							<p class="fh5co-social-icons">
								<a href="#"><i class="icon-twitter2"></i></a>
								<a href="#"><i class="icon-facebook2"></i></a>
								<a href="#"><i class="icon-instagram"></i></a>
								<a href="#"><i class="icon-dribbble2"></i></a>
								<a href="#"><i class="icon-youtube"></i></a>
							</p>
							<p>Copyright 2021 <a href="#">Module</a>. All Rights Reserved. <br>Made with <i class="icon-heart3"></i> by Groupe 3 </p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		
		<!-- Modales -->
		
		<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">S'inscrire</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <i class="fas fa-user prefix grey-text"></i>
          <input type="text" id="orangeForm-name" class="form-control validate">
          <label data-error="wrong" data-success="right" for="orangeForm-name">Votre nom</label>
        </div>
        <div class="md-form mb-5">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="email" id="orangeForm-email" class="form-control validate">
          <label data-error="wrong" data-success="right" for="orangeForm-email">Votre email</label>
        </div>

        <div class="md-form mb-4">
          <i class="fas fa-lock prefix grey-text"></i>
          <input type="password" id="orangeForm-pass" class="form-control validate">
          <label data-error="wrong" data-success="right" for="orangeForm-pass">Votre mot de passe</label>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-primary btn-block">S'inscrire</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Connecter</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="email" id="defaultForm-email" class="form-control validate">
          <label data-error="wrong" data-success="right" for="defaultForm-email">Votre email</label>
        </div>

        <div class="md-form mb-4">
          <i class="fas fa-lock prefix grey-text"></i>
          <input type="password" id="defaultForm-pass" class="form-control validate">
          <label data-error="wrong" data-success="right" for="defaultForm-pass">Votre mot de passe</label>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-primary btn-block">Connexion</button>
      </div>
    </div>
  </div>
</div>
