<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<header id="fh5co-header-section" class="sticky-banner">
<div class="container">
        <div class="nav-header">
            <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>

            <h1 id="fh5co-logo"><a href="<%=request.getContextPath()%>/admin/admin"><img src="images/icon.png" alt="RIM-Aire" style="height: 3.8rem;"> RIM Aire</a></h1>
            <!-- START #fh5co-menu-wrap <i class="icon-airplane">-->
            <nav id="fh5co-menu-wrap" role="navigation">
                <ul class="sf-menu" id="fh5co-primary-menu">
                    <c:choose>
                        <c:when test="${sessionScope.page=='admin' }"><li class="active"> <a href="<%=request.getContextPath()%>/admin">dashboard</a></li></c:when>
                        <c:otherwise><li > <a href="<%=request.getContextPath()%>/admin">dashboard</a></li></c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.page=='Vols' }">
                            <li class="active">
                                <a href="" class="fh5co-sub-ddown">ajouter</a>
                                <ul class="fh5co-sub-menu">
                                    <li class="active"><a href="<%=request.getContextPath()%>/vols">vols</a></li>
                                    <li ><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
                                    <li ><a href="<%=request.getContextPath()%>/Location">Voiture</a></li>
                                </ul>
                            </li>
                        </c:when>
                        <c:when test="${sessionScope.page=='hotel' }">
                            <li class="active">
                                <a href="" class="fh5co-sub-ddown">ajouter</a>
                                <ul class="fh5co-sub-menu">
                                    <li ><a href="<%=request.getContextPath()%>/Vols">vol</a></li>
                                    <li class="active"><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
                                    <li ><a href="<%=request.getContextPath()%>/Location">Voiture</a></li>
                                </ul>
                            </li>
                        </c:when>
                        <c:when test="${sessionScope.page=='Location' }">
                            <li class="active">
                                <a href="" class="fh5co-sub-ddown">ajouter</a>
                                <ul class="fh5co-sub-menu">
                                    <li ><a href="<%=request.getContextPath()%>/Vols" class="btn btn-default btn-rounded mb-6" data-toggle="modal" data-target="#modalLoginForm">vol</a></li>
                                    <li ><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
                                    <li class="active"><a href="/Location">Voiture</a></li>
                                </ul>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li >
                                <a href="" class="fh5co-sub-ddown">ajouter</a>
                                <ul class="fh5co-sub-menu">
                                    <li ><a href="<%=request.getContextPath()%>/Vols">vol</a></li>
                                    <li ><a href="<%=request.getContextPath()%>/hotel">Hotel</a></li>
                                    <li ><a href="<%=request.getContextPath()%>/Location">Voiture</a></li>
                                </ul>
                            </li>
                        </c:otherwise>
                    </c:choose>

                    <c:choose>
                        <c:when test="${sessionScope.page=='Vols' }"><li class="active"><a href="<%=request.getContextPath()%>/admin/List_Vols">Vols</a></li></c:when>
                        <c:otherwise><li ><a href="<%=request.getContextPath()%>/admin/List_Vols">Vols</a></li></c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.page=='booking' }"><li class="active"><a href="<%=request.getContextPath()%>/admin/reservation">Booking</a></li></c:when>
                        <c:otherwise><li ><a href="<%=request.getContextPath()%>/admin/reservation">Booking</a></li></c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.page=='Avions' }"><li class="active"><a href="<%=request.getContextPath()%>/admin/avions">Avions</a></li></c:when>
                        <c:otherwise><li ><a href="<%=request.getContextPath()%>/admin/avions">Avions</a></li></c:otherwise>
                        </c:choose>


                    <li><a class="btn btn-default btn-rounded mb-6" data-toggle="modal" data-target="#changepsw">PROFILE</a></li>
                    <li> <a href="<%=request.getContextPath()%>/index" class="btn btn-default btn-rounded mb-4" >DECONNECTER</a></li>

                    <!-- <li><a href="" class="btn btn-default btn-rounded my-3" data-toggle="modal" data-target="#modalLRForm">Connecter &amp; Enregistrer</a></li> -->
                </ul>
            </nav>
        </div>
    </div>            
</header>
<%-- 		class='<% session.getAttribute("page").toString().equals("index")? "active" : ""; %>' --%>