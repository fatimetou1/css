<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<jsp:include page="include/meta.jsp"></jsp:include>
<jsp:include page="include/header.jsp"></jsp:include>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>
<% session.setAttribute("page", "flight"); %>
	<div id="fh5co-wrapper">
		<div id="fh5co-page">

			<jsp:include page="include/common_files/page_heading.jsp"></jsp:include>

			<!-- end:header-top -->

			<jsp:include page="include/common_files/booking_content.jsp"></jsp:include>


			<div id="fh5co-tours" class="fh5co-section-gray">
				<div class="container">
					<div class="row">
						<div
							class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
							<h3>R�server un vol</h3>
							<p>Far far away, behind the word mountains, far from the
								countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
						
						<div class="card-body mb-3">
							<c:set var="nrow" value="0"></c:set>
				 		 	<table class="table table-light table-borderless text-center" id="listresto" width="100%" >
				 		 		<tr>
				 		 		<% int nrow = 0; %>
								<c:forEach var="fly" items="${listAlltheflights}">
									<% if(nrow >= 4){ %> </tr><% nrow =0;} %>
									<% if(nrow == 0){ %> <tr> <% } %>
									
									<td>
									<div style="background-color:rgba(0, 0, 0, 0.06);" class="card shadow shadow-sm">
									 <!-- bg-transparent -->
									  <div class="card-header border-success"><c:out value="${fly.airplanename}" /></div>
									  <div class="card-body text-success">
									    <h4 class="card-title"><c:out value="${fly.fnum}" /></h4>
									    <p class="card-text"></p>
									    <div class="col-sm-6"><span class="float-left">Departure:</span></div>
									    <div class="col-sm-6 " ><span class="float-left">Arrive:</span></div>
									 	<div class="col-sm-6"><span > <c:out value="${fly.depCity}" /></span> 
										 </div>
										 
										 <div class="col-sm-6">
										 <span > <c:out value="${fly.arvCity}" /> </span>
										 </div>
										 <div class="col-sm-6">
										<span> <c:out value="${fly.depDate}" /> </span>
										</div>
										<div class="col-sm-6">
										<span > <c:out value="${fly.arvDate}" /> </span>
										</div>
										<div class="col-sm-12">
										<span>prix: <c:out value="${fly.prix}" /> MRU </span>
										</div>
										<div class="col-sm-6">
										<span>Repas gratuits : <c:out value="${fly.free_meals ? 'Oui' : 'Non'}" /> </span>
										</div>
										<div class="col-sm-6">
										<span>Remboursable :  <c:out value="${fly.refundable ? 'Oui' : 'Non'}" /> </span>
										</div>
									  </div>
									  <div class="card-footer border-success"><a href="" class="btn btn-primary">Reserver</a></div>
									</div>
									</td>
									<% nrow++; %>
								</c:forEach>
							
							</table>
						</div>
						
						
					</div>
					<div class="row row-bottom-padded-md">
						<div class="col-md-12 animate-box">
							<h2 class="heading-title">Offre de vol du jour</h2>
						</div>
						<div class="col-md-6 animate-box">
							<div class="row">
								<div class="col-md-12">
									<h4>De meilleures offres, plus de capacit�s</h4>
									<p>Far far away, behind the word mountains, far from the
										countries Vokalia and Consonantia, there live the blind texts.</p>
								</div>
								<div class="col-md-12">
									<h4>Tenez-vous au courant de l'actualit� de votre compagnie a�rienne</h4>
									<p>Far far away, behind the word mountains, far from the
										countries Vokalia and Consonantia, there live the blind texts.</p>
								</div>
								<div class="col-md-12">
									<h4>Exp�rience en vol</h4>
									<p>Far far away, behind the word mountains, far from the
										countries Vokalia and Consonantia, there live the blind texts.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 animate-box">
							<img class="img-responsive" src="images/cover_bg_3.jpg"
								alt="travel"> <a href="#" class="flight-book">
								<div class="plane-name">
									<span class="p-flight">United States Airways</span>
								</div>
								<div class="desc">
									<div class="left">
										<h4>HK-MNL</h4>
										<span>Dec 20 - Dec 29</span>
									</div>
									<div class="right">
										<span class="price"> <i class="icon-arrow-down22"></i>
											36,150 N-UM ($1,000)
										</span>
									</div>
								</div>
							</a> <a href="#" class="flight-book">
								<div class="plane-name">
									<span class="p-flight">Qatar Airways</span>
								</div>
								<div class="desc">
									<div class="left">
										<h4>HK-MNL</h4>
										<span>Dec 20 - Dec29</span>
									</div>
									<div class="right">
										<span class="price"> <i class="icon-arrow-down22"></i>
											28,543 N-UM ($790)
										</span>
									</div>
								</div>
							</a> <a href="#" class="flight-book">
								<div class="plane-name">
									<span class="p-flight">Philippine Airline</span>
								</div>
								<div class="desc">
									<div class="left">
										<h4>MNL-HK</h4>
										<span>Dec 20 - Dec29</span>
									</div>
									<div class="right">
										<span class="price"> <i class="icon-arrow-down22"></i>
											18,065  N-UM ($500)
										</span>
									</div>
								</div>
							</a> <a href="#" class="flight-book">
								<div class="plane-name">
									<span class="p-flight">China Airways</span>
								</div>
								<div class="desc">
									<div class="left">
										<h4>HK-LAS</h4>
										<span>Dec 20 - Dec29</span>
									</div>
									<div class="right">
										<span class="price"> <i class="icon-arrow-down22"></i>
											32,517  N-UM ($900)
										</span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="row row-bottom-padded-md">
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-1.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>New York</h3>
									<span>3 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-2.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Philippines</h3>
									<span>4 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-3.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Hongkong</h3>
									<span>2 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant <i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<!-- <div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-4.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>New York</h3>
									<span>3 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-5.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Philippines</h3>
									<span>4 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 fh5co-tours animate-box"
							data-animate-effect="fadeIn">
							<div href="#">
								<img src="images/place-6.jpg"
									alt=""
									class="img-responsive">
								<div class="desc">
									<span></span>
									<h3>Hongkong</h3>
									<span>2 nuits</span> <span class="price">36,150 N-UM ($1,000)</span> <a
										class="btn btn-primary btn-outline" href="#">Reserve maintenant<i
										class="icon-arrow-right22"></i></a>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
					<!-- fh5co-blog-section -->
		<jsp:include page="include/common_files/client_review.jsp"></jsp:include>

		<!-- Footer section -->
		<jsp:include page="include/common_files/page_footing.jsp"></jsp:include>


		</div>
		<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->
	<script type="text/javascript">
		/* $(document).ready(function(){
			$("#tablediv").hide();
			$("#listtheflight").click(function(event){
				$.get('flight',function(responseJson){
					if(responseJson!=null){
						$("#flighttable").find("tr:gt(0)").remove();
						var tableflight = $("#flighttable");
						$.each(responseJson,function(key,value){
							var rowNew = $("<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
							rowNew.chiledren().eq(0).tex(value('id'));
							rowNew.chiledren().eq(1).tex(value('flight_number'));
							rowNew.chiledren().eq(2).tex(value('airplane_id'));
							rowNew.chiledren().eq(3).tex(value('d_city_id'));
							rowNew.chiledren().eq(4).tex(value('d_date'));
							rowNew.chiledren().eq(5).tex(value('a_city_id'));
							rowNew.chiledren().eq(6).tex(value('a_date'));
							rowNew.chiledren().eq(7).tex(value('price'));
							rowNew.chiledren().eq(8).tex(value('fee_meals'));
							rowNew.chiledren().eq(9).tex(value('refundable'));
							rowNew.appendTo(tableflight);
						});
					}	
				});
				$("#tablediv").show();
			});	
		}); */
	</script>
	<!-- jQuery -->
	<jsp:include page="include/footer.jsp"></jsp:include>

</body>
</html>

