<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <jsp:include page="include/meta.jsp"></jsp:include>
        <jsp:include page="include/header.jsp"></jsp:include>
            <link rel="stylesheet" href="js/dataTables/dataTables.bootstrap.css">
            <!-- FOR IE9 below -->
            <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
            <![endif]-->

        </head>
        <body>
        <% session.setAttribute("page", "Vols");%>
        <div id="fh5co-wrapper">
            <div id="fh5co-page">

                <jsp:include page="include/common_files/admin_heading.jsp"></jsp:include>

                    <!-- end:header-top -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col col-xs-6">
                                    <h3 class="panel-title" align="right">D�tails Des Vols</h3>
                                </div>
                                <div class="col col-xs-6 text-right">
                                    <button type="button" class="btn btn-sm btn-primary btn-create" data-toggle="modal" data-target="#create" data-title="create">Ajouter un vol</button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="Vols">
                                    <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label><select name="dataTables-example_length" aria-controls="dataTables-example" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> Vols par page</label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm" aria-controls="dataTables-example"></label></div></div></div><table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" aria-describedby="dataTables-example_info">
                                            <thead>
                                                <tr role="row"><th><em class="fa fa-cog"></em></th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 303px;">Code</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 303px;">AvionID</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 275px;">Depart</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 165px;">Arrive</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 117px;">DateD</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 117px;">DateA</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 117px;">Capacite</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 117px;">Repas_Gratuits</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 275px;">remboursable</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 165px;">PlacesOcupe</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 117px;">Status</th></tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <<td align="center">
                                                        <button class="btn btn-primary btn-xs" onclick="editEmp()" data-toggle="modal" data-target="#editemp" data-title="Edit"><em class="glyphicon glyphicon-pencil"></em></button>
                                                        <button class="btn btn-danger btn-xs" onclick="deleteemp()" data-toggle="modal" data-target="#delemp" data-title="Delete"><em class="glyphicon glyphicon-trash"></em></button>
                                                    </td>
                                                    <<td>cell</td>
                                                    <<td>cell</td>
                                                    <<td>cell</td>
                                                    <<td>cell</td>
                                                    <<td>cell</td>
                                                    <<td>cell</td>
                                                    <<td>cell</td>
                                                    <<td>cell</td>
                                                    <<td>cell</td>
                                                    <<td>cell</td>
                                                    <<td><span class="label label-success">Approved</span></td>
                                                </tr>
                                            </tbody>
                                        </table><div class="row"><div class="col-sm-6"><div class="dataTables_info" id="dataTables-example_info" role="alert" aria-live="polite" aria-relevant="all">Affichage de 1 � 10 sur 57 entr�es</div></div><div class="col-sm-6"><div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_previous"><a href="#">Pr�c�dent</a></li><li class="paginate_button active" aria-controls="dataTables-example" tabindex="0"><a href="#">1</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">2</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">3</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">4</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">5</a></li><li class="paginate_button " aria-controls="dataTables-example" tabindex="0"><a href="#">6</a></li><li class="paginate_button next" aria-controls="dataTables-example" tabindex="0" id="dataTables-example_next"><a href="#">Suivant </a></li></ul></div></div></div></div>
                                </table>
                            </div>

                        </div>
                        <!-- Footer section -->
                    <jsp:include page="include/common_files/admin_footing.jsp"></jsp:include>


                    </div>
                    <!-- END fh5co-page -->


                </div>
                <div class="modal fade" id="create" tabindex="-1" role="dialog"
                     aria-labelledby="add" aria-hidden="true"><div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                                <h4 class="modal-title custom_align" id="Heading">Ajouter un vol</h4>


                                <div class="row">
                                    <div class="col-xxs-12 col-xs-12 mt" >
                                        <label for="from">Numero de l'avion:</label>
                                        <input type="text" class="form-control" id="NumVol" placeholder="NJ-XXXX"/>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt">
                                        <div class="input-field">
                                            <label for="from">Ville De Deppart:</label>
                                            <input type="text" class="form-control" id="VilleD" placeholder="Nouakchott, MR"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt">
                                        <div class="input-field">
                                            <label for="from">Ville d'arrive:</label>
                                            <input type="text" class="form-control" id="VilleA" placeholder="Tokyo, Japan"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt alternate">
                                        <div class="input-field">
                                            <label for="date-start">Date de depart:</label>
                                            <input type="text" class="form-control" id="date-start" placeholder="mm/dd/yyyy"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt alternate">
                                        <div class="input-field">
                                            <label for="date-end">Date de l'arrive</label>
                                            <input type="text" class="form-control" id="date-end" placeholder="mm/dd/yyyy"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt alternate">
                                        <div class="input-field">
                                            <label for="time-start">Heure de depart:</label>
                                            <input type="time" class="form-control" id="time-start" placeholder="XX:XX:XX"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt alternate">
                                        <div class="input-field">
                                            <label for="time-end">Heure de l'arrive</label>
                                            <input type="time" class="form-control" id="time-end" placeholder="XX:XX:XX"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-12 mt" >
                                        <label for="from">Capacite Total:</label>
                                        <input type="number" class="col-xxs-12 col-xs-12 mt" id="Capacite" placeholder="XXXXXXX"/>
                                    </div>
                                    <div class="col-xxs-12 col-xs-12 mt" >
                                        <div class="col-xxs-12 col-xs-8 mt" >
                                            <label for="from">remboursable:</label>
                                        </div>
                                        <div class='col-xxs-12 col-xs-2 mt'>
                                            <select class="cs-select cs-skin-border">
                                                <option value="0" disabled selected id ="refundable">NOM</option>
                                                <option value="0">NOM</option>
                                                <option value="1">OUI</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-12 mt" >
                                        <div class="col-xxs-12 col-xs-8 mt" >
                                            <label for="from">Repas_Gratuits</label>
                                        </div>
                                        <div class='col-xxs-12 col-xs-2 mt'>
                                            <select class="cs-select cs-skin-border" id="free_meal">
                                                <option value="0" disabled selected>NOM</option>
                                                <option value="0">NOM</option>
                                                <option value="1">OUI</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div >
                                        <div class="col-xxs-12 col-xs-12 mt" >
                                            <div class="col-md-4"><label for="from">Classe Economique Qt:</label></div>
                                            <div class="col-xxs-12 col-xs-8 mt"><input type="number" class="form-control" id="from-place" placeholder="Qt de places"/></div>
                                        </div>
                                        <div class="col-xxs-12 col-xs-10 mt" >
                                            <label for="from">Prix:</label>
                                            <input type="number" class="form-control" id="PriE" placeholder="Prix"/>
                                        </div>
                                        <div class='col-xxs-12 col-xs-1 mt-1'>
                                            <label for="from">Devise</label>
                                            <select class="cs-select cs-skin-border">
                                                <option value="" disabled selected>MRU</option>
                                                <option value="1">MRU</option>
                                                <option value="2">USD</option>
                                                <option value="3">EUR</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div >
                                        <div class="col-xxs-12 col-xs-12 mt" >
                                            <div class="col-md-4"><label for="from">Classe Business Qt:</label></div>
                                            <input type="number" class="form-control" id="from-place" placeholder="Qt de Places"/>
                                        </div>
                                        <div class="col-xxs-12 col-xs-10 mt" >
                                            <label for="from">Prix:</label>
                                            <input type="number" class="form-control" id="PrixB" placeholder="Prix"/>
                                            <div class='col-xxs-12 col-xs-1 mt-1'>
                                                <label for="from">Devise</label>
                                                <select class="cs-select cs-skin-border">
                                                    <option value="" disabled selected>MRU</option>
                                                    <option value="1">MRU</option>
                                                    <option value="2">USD</option>
                                                    <option value="3">EUR</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                    <div >
                                        <div class="col-xxs-12 col-xs-12 mt" >
                                            <div class="col-md-4"><label for="from">Classe Premiere Classe Qt:</label></div>
                                            <div class="col-xxs-12 col-xs-8 mt"><input type="number" class="form-control" id="from-place" placeholder="Qt de place"/></div>
                                        </div>
                                        <div class="col-xxs-12 col-xs-10 mt" >
                                            <label for="from">Prix:</label>
                                            <input type="number" class="form-control" id="PrixF" placeholder="Prix"/>
                                        </div>
                                        <div class='col-xxs-12 col-xs-1 mt-1'>
                                            <label for="from">Devise</label>
                                            <select class="cs-select cs-skin-border">
                                                <option value="" disabled selected>MRU</option>
                                                <option value="1">MRU</option>
                                                <option value="2">USD</option>
                                                <option value="3">EUR</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="modal-footer ">
                                        <button type="submit" class="btn btn-primary btn-block"
                                                style="width: 100%;">
                                            <span class="glyphicon glyphicon-ok-sign"></span> Mettre � jour
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="editemp" tabindex="-1" role="dialog"
                     aria-labelledby="edit" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                                <h4 class="modal-title custom_align" id="Heading">Vol
                                    Details</h4>

                                <div class="row">
                                    <div class="col-xxs-12 col-xs-12 mt" >
                                        <label for="from">Numero de l'avion:</label>
                                        <input type="text" class="form-control" id="NumVol" placeholder="NJ-XXXX"/>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt">
                                        <div class="input-field">
                                            <label for="from">Ville De Deppart:</label>
                                            <input type="text" class="form-control" id="VilleD" placeholder="Nouakchott, MR"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt">
                                        <div class="input-field">
                                            <label for="from">Ville d'arrive:</label>
                                            <input type="text" class="form-control" id="VilleA" placeholder="Tokyo, Japan"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt alternate">
                                        <div class="input-field">
                                            <label for="date-start">Date de depart:</label>
                                            <input type="text" class="form-control" id="date-start" placeholder="mm/dd/yyyy"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt alternate">
                                        <div class="input-field">
                                            <label for="date-end">Date de l'arrive</label>
                                            <input type="text" class="form-control" id="date-end" placeholder="mm/dd/yyyy"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt alternate">
                                        <div class="input-field">
                                            <label for="time-start">Heure de depart:</label>
                                            <input type="time" class="form-control" id="time-start" placeholder="XX:XX:XX"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-6 mt alternate">
                                        <div class="input-field">
                                            <label for="time-end">Heure de l'arrive</label>
                                            <input type="time" class="form-control" id="time-end" placeholder="XX:XX:XX"/>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-12 mt" >
                                        <label for="from">Capacite Total:</label>
                                        <input type="number" class="col-xxs-12 col-xs-12 mt" id="Capacite" placeholder="XXXXXXX"/>
                                    </div>
                                    <div class="col-xxs-12 col-xs-12 mt" >
                                        <div class="col-xxs-12 col-xs-8 mt" >
                                            <label for="from">remboursable:</label>
                                        </div>
                                        <div class='col-xxs-12 col-xs-2 mt'>
                                            <select class="cs-select cs-skin-border">
                                                <option value="0" disabled selected id ="refundable">NOM</option>
                                                <option value="0">NOM</option>
                                                <option value="1">OUI</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxs-12 col-xs-12 mt" >
                                        <div class="col-xxs-12 col-xs-8 mt" >
                                            <label for="from">Repas_Gratuits</label>
                                        </div>
                                        <div class='col-xxs-12 col-xs-2 mt'>
                                            <select class="cs-select cs-skin-border" id="free_meal">
                                                <option value="0" disabled selected>NOM</option>
                                                <option value="0">NOM</option>
                                                <option value="1">OUI</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div >
                                        <div class="col-xxs-12 col-xs-12 mt" >
                                            <div class="col-md-4"><label for="from">Classe Economique Qt:</label></div>
                                            <div class="col-xxs-12 col-xs-8 mt"><input type="number" class="form-control" id="from-place" placeholder="Qt de places"/></div>
                                        </div>
                                        <div class="col-xxs-12 col-xs-10 mt" >
                                            <label for="from">Prix:</label>
                                            <input type="number" class="form-control" id="PriE" placeholder="Prix"/>
                                        </div>
                                        <div class='col-xxs-12 col-xs-1 mt-1'>
                                            <label for="from">Devise</label>
                                            <select class="cs-select cs-skin-border">
                                                <option value="" disabled selected>MRU</option>
                                                <option value="1">MRU</option>
                                                <option value="2">USD</option>
                                                <option value="3">EUR</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div >
                                        <div class="col-xxs-12 col-xs-12 mt" >
                                            <div class="col-md-4"><label for="from">Classe Business Qt:</label></div>
                                            <input type="number" class="form-control" id="from-place" placeholder="Qt de Places"/>
                                        </div>
                                        <div class="col-xxs-12 col-xs-10 mt" >
                                            <label for="from">Prix:</label>
                                            <input type="number" class="form-control" id="PrixB" placeholder="Prix"/>
                                            <div class='col-xxs-12 col-xs-1 mt-1'>
                                                <label for="from">Devise</label>
                                                <select class="cs-select cs-skin-border">
                                                    <option value="" disabled selected>MRU</option>
                                                    <option value="1">MRU</option>
                                                    <option value="2">USD</option>
                                                    <option value="3">EUR</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div >
                                        <div class="col-xxs-12 col-xs-12 mt" >
                                            <div class="col-md-4"><label for="from">Classe Premiere Classe Qt:</label></div>
                                            <div class="col-xxs-12 col-xs-8 mt"><input type="number" class="form-control" id="from-place" placeholder="Qt de place"/></div>
                                        </div>
                                        <div class="col-xxs-12 col-xs-10 mt" >
                                            <label for="from">Prix:</label>
                                            <input type="number" class="form-control" id="PrixF" placeholder="Prix"/>
                                        </div>
                                        <div class='col-xxs-12 col-xs-1 mt-1'>
                                            <label for="from">Devise</label>
                                            <select class="cs-select cs-skin-border">
                                                <option value="" disabled selected>MRU</option>
                                                <option value="1">MRU</option>
                                                <option value="2">USD</option>
                                                <option value="3">EUR</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                            </div>


                            <div class="modal-footer ">
                                <button type="submit" class="btn btn-warning btn-lg"
                                        style="width: 100%;">
                                    <span class="glyphicon glyphicon-ok-sign"></span> Mettre � jour
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <div class="modal fade" id="delemp" tabindex="-1" role="dialog"
                     aria-labelledby="edit" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                                <h4 class="modal-title custom_align" id="Heading">Supprimer cette entr�e</h4>
                            </div>
                            <div class="modal-body">

                                <div class="alert alert-danger">
                                    <span class="glyphicon glyphicon-warning-sign"></span>
                                    s�r que vous voulez supprimer cet enregistrement?
                                </div>

                            </div>
                            <div class="modal-footer ">
                                <button type="button" class="btn btn-success">
                                    <span class="glyphicon glyphicon-ok-sign"></span> Oui
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span> Non
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>

                </div>
                <!-- END fh5co-wrapper -->

                <!-- jQuery -->

                <script src="js/dataTables/jquery.dataTables.js"></script>
                <script src="js/dataTables/dataTables.bootstrap.js"></script>
                
            <jsp:include page="include/footer.jsp"></jsp:include>

    </body>
</html>

