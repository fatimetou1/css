<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
	<jsp:include page="include/meta.jsp"></jsp:include>
	<jsp:include page="include/header.jsp"></jsp:include>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>
<% session.setAttribute("page", "contact"); %>
	<div id="fh5co-wrapper">
		<div id="fh5co-page">

			<jsp:include page="include/common_files/page_heading.jsp"></jsp:include>

			<!-- end:header-top -->

			<jsp:include page="include/common_files/booking_content.jsp"></jsp:include>


			<div id="fh5co-contact" class="fh5co-section-gray">
				<div class="container">
					<div class="row">
						<div
							class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
							<h3>Informations de contact</h3>
							<p>Far far away, behind the word mountains, far from the
								countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
					<form action="#">
						<div class="row animate-box">
							<div class="col-md-6">
								<h3 class="section-title">Notre adresse</h3>
								<p>Far far away, behind the word mountains, far from the
									countries Vokalia and Consonantia, there live the blind texts.</p>
								<ul class="contact-info">
									<li><i class="icon-location-pin"></i>201 Rue NDB ,
										Tegragh zeina , Nouakchott</li>
									<li><i class="icon-phone2"></i>+ 222 20 20 18 17 </li>
									<li><i class="icon-mail"></i><a href="#">abdouy71@gmail.com</a></li>
									<li><i class="icon-globe2"></i><a href="#">www.rimaireline.com</a></li>
								</ul>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Name">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Email">
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<textarea name="" class="form-control" id="" cols="30"
												rows="7" placeholder="Message"></textarea>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<input type="submit" value="Send Message"
												class="btn btn-primary">
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- blog section -->
			<jsp:include page="include/common_files/client_review.jsp"></jsp:include>
			
			<!-- map section -->
			<!--  <div id="map" class="fh5co-map"></div> -->
			<!-- END map -->

			<!-- Footer section -->
			<jsp:include page="include/common_files/page_footing.jsp"></jsp:include>



		</div>
		<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->


	<jsp:include page="include/footer.jsp"></jsp:include>

</body>
</html>

